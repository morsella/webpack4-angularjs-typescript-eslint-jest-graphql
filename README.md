Webpack 4 boilerplate setup with AngularJS, TypeScript, ESlint, GraphQL, ApolloClient, Jest and MockServer using Astrograph Public API...

Astrograph provides very good documented API. Unfortunately the data that is received is not consistent. For example when I could fetch transactions one day, the next day the API would return an error saying that some data can't be fetched. I tried to make the boiler plate with all the updated and recent dependencies. The only issue that became more problematic is that Jest fails to read .graphql files. I tried to do all the different configurations and ignore directories or files, but that is not something that works yes smoothly. I spent over 24 hours reading and trying different configurations setting, and it seems that this is known issue. I really hope that soon there will be updates from Jest, GraphQL or Webpack to solve this. TypeScript is set to strict mode and all the Angular-js 1.7.. were written in TypeScript. TypeScript is powerful and makes it more secure and efficient in OOP. With the new updates that Angular community releases, I like the adaptivity and simplicity and it reminds me a bit of React. I didn't include RxJs in this setup - but it is something that definitely can be added, depends on project growth. 

I keep this little project public for anyone to use it. 

There are 3 simple scripts in package.json
- npm run build - build the package and also include TypesScript checks
- npm run lint - eslint tests 
- npm run test - jest test

link to the working app: https://grql.moresella.nl/astrographql/

You are welcome to clone the repository and use it.
Keep it simple and keep coding :) 

M. Sella