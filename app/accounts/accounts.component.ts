import angular from 'angular';
import AccountsController from './accounts.controller';
import AccountService from '../account/account.service';

export default angular.module('accountsModule').factory('accountService', AccountService)
                            .controller('AccountsController',($scope, $rootScope) => new AccountsController($scope, $rootScope))
                            .component('accountsComponent', {
                                bindings:{
                                    loaded: '&'
                                },
                                template:`<div>
                                <account-component init-account="ctrl.initAccount" init-account-id="GB6NVEN5HSUBKMYCE5ZOWSK5K23TBWRUQLZY3KNMXUZ3AQ2ESC4MY4AQ"/>
                                </div>
                                <div ng-controller="AccountsController as ctrl" ng-init="ctrl.onInit()">
                                    <div ng-if="!ctrl.loading" class="accounts_box col-md-12">
                                        <h4 class="col-md-12"> Total accounts: {{ctrl.accounts.length}} </h4>
                                        <div ng-repeat="(i, account) in ctrl.accounts" class="flex col-md-12" ng-click="ctrl.selectAccount(account.id, i)" 
                                            ng-class="{'active': account.id===ctrl.initAccount.id}">
                                            <div class="flex-item"> account number: {{account.sequenceNumber}}
                                            <div ng-repeat="balance in account.balances" class="account_balances">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <th>asset</th>
                                                        <th>balance</th>
                                                    </tr>
                                                    <tr>
                                                        <td>{{balance.asset.code}}</td>
                                                        <td>{{balance.balance}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <strong ng-if="ctrl.error.length > 1" class="error"><p>ERROR: {{ctrl.error}}</p></strong>
                                </div>
                                `,
                                controller: AccountsController
                            });