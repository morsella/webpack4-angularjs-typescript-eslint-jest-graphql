import AccountService from "../account/account.service";
import { AccountType } from "../interfaces/Account.type";
import { testData } from '../test/test.dataAccounts';
import { testDataAccount } from '../test/test.dataAccount';
import { IRootScopeService } from "angular";

class AccountsController {
   private parent: ng.IController;
   private $scope: any;
   private $rootScope: IRootScopeService;
   private _accountService: AccountService;
    accounts = [] as AccountType[];
    initAccount = {} as AccountType;
    loading: boolean = true;
    header: string = 'random account:';
    selectTotal: number = 10;
    error: string = '';
    private elmTop = 0;
    constructor($scope: any, $rootScope: IRootScopeService){
        this._accountService = new AccountService();
        this.$scope = $scope
        this.parent = this.$scope.$parent.$ctrl;
        this.$rootScope = $rootScope;
    }
    onInit(): void {
        console.log('Accounts init');
        this.getData(this.selectTotal);
    }
	getData(select: number): void {
        this._accountService.getData(select).then(res => {
            if(res){
                console.log('res', res);
                this.accounts = res.accounts.nodes;
                this.reset();
                this.elmTop = this.getTopElem('selectedAccount');
            } 
        }).catch(err => {
            if(err.message.includes('GraphQL error: connect' || 'ApolloError' )){
                console.log(err);
                this.getMockData();
            } 
            else{
                console.log('ERROR: ', err);
                this.getMockData();
            }
        });
    }
    selectAccount(id: string, i: number): void {
        this._accountService.getAccountThresholds(id).then(res => {
            if(res && res.id ){
                const updatedItems = this.accounts.map((acc) =>
                    acc.id === id ? {...acc, numSubentries: res.numSubentries, thresholds: res.thresholds} : acc);
                updatedItems.unshift(updatedItems.splice(i, 1)[0]);
                this.initAccount = updatedItems.filter((acc) => acc.id === id)[0];
                this.accounts = updatedItems;
            }else{
                this.initAccount = this.accounts.filter(a => a.id === id)[0];
            }
            this._accountService.selectedAccount(this.initAccount, this.$rootScope);
            this.scrollToTop();
        }).catch(err => {
            console.log('ERROR: ', err);
            this.initAccount = this.accounts.filter(a => a.id === id)[0];
        });
    }
    private getMockData(): void {
        this.accounts = testData.accounts.nodes;
        this.initAccount = testDataAccount.account;
        this.selectTotal = this.accounts.length;
        this.reset();
        this.error = 'This is mock data - Astrograph demo server is down';
    }
    private reset(): void {
        this.error = '';
        this.loading = false;
        this.parent.loaded();
    }
    private getTopElem(elem: string): number {
        let elScreenY: any
        document.getElementById(elem) ? elScreenY = document.getElementById(elem)?.offsetTop : 0;
        return elScreenY;
    }
    private scrollToTop(): void {
        // const intervalScroll = setInterval(() : void=>{
        //     window.scroll({top: this.elmTop - 20, left: 0,  behavior: 'smooth'});
        //     if( window.scrollY < this.elmTop + 20 || window.scrollY > this.elmTop - 20) clearInterval(intervalScroll);
        // },700);
        //TODO scroll fix for Safari
        window.scroll(0, this.elmTop - 20);
    }
}
export default AccountsController;