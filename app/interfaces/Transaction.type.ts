import { OperationType } from './Operation.type';
export interface TransactionType {
   readonly id: string;
   readonly feeAmount: number;
   readonly success: boolean;
   index: number;
   readonly operations: {
      readonly nodes: OperationType[];
    }
}