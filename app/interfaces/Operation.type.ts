export interface OperationType {
   readonly id: string;
   readonly type: string;
   readonly dateTime: string;
}