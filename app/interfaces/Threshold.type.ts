export interface ThresholdType {
     low: number;
     high: number;
}