export interface AssetCodeType {
    readonly code: string;
}