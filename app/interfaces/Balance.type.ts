import { AssetType } from './Asset.type';
export interface BalanceType {
    readonly balance: string;
    readonly limit: string;
    readonly asset: AssetType;
    readonly authorized: boolean;
}