import { AccountType } from "./Account.type";

export interface AccountsType {
    readonly accounts: {
       readonly nodes: AccountType[];
    };
}
export interface DataType {
   readonly data: AccountsType;
}