import { BalanceType } from "./Balance.type";
import { TransactionType } from './Transaction.type';
import { ThresholdType } from "./Threshold.type";

export interface AccountType {
   readonly id: string;
   readonly sequenceNumber: string;
   readonly balances : BalanceType[];
   readonly transactions: {
       readonly nodes:  TransactionType[];
    };
    numSubentries: number;
    thresholds: ThresholdType;

}
