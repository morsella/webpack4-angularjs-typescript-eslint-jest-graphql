export const testDataAccount: any = {
    account: {
      id: "GB6NVEN5HSUBKMYCE5ZOWSK5K23TBWRUQLZY3KNMXUZ3AQ2ESC4MY4AQ",
      sequenceNumber: "3940598134341708",
      numSubentries: 1,
      thresholds: {
        low: 1,
        high: 5
      },
      balances: [
        {
          asset: {
            issuer: null,
            code: "XLM"
          },
          balance: "2626443193.8155707",
          limit: "922337203685.4775807",
          authorized: true
        }
      ]
    }
}
