const typeDefs = `
type Account {
    id: AccountID!
    sequenceNumber: String!
    numSubentries: Int!
    reservedBalance: String!
    inflationDestination: Account
    homeDomain: String
    thresholds: AccountThresholds!
    signers: [Signer]
    ledger: Ledger!
    data: [DataEntry]
    assets(first: Int, after: String, last: Int, before: String): AssetConnection
    balances: [Balance]
    operations(
      first: Int
      after: String
      last: Int
      before: String
      order: Order
    ): OperationConnection
    payments(
      first: Int
      after: String
      last: Int
      before: String
    ): OperationConnection
    trades(first: Int, after: String, last: Int, before: String): TradeConnection
    transactions(
      first: Int
      last: Int
      before: String
      after: String
    ): TransactionConnection
    offers(
      selling: AssetID
      buying: AssetID
      first: Int
      after: String
      last: Int
      before: String
    ): OfferConnection
  }
  
  type AccountConnection {
    pageInfo: PageInfo!
    nodes: [Account]
    edges: [AccountEdge]
  }
  
  type AccountEdge {
    cursor: String!
    node: Account
  }
  
  scalar AccountID
  
  type AccountMergeOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    destination: Account!
  }
  
  type AccountSubscriptionPayload {
    id: AccountID!
    mutationType: MutationType!
    values: AccountValues
  }
  
  type AccountThresholds {
    masterWeight: Int!
    low: Int!
    medium: Int!
    high: Int!
  }
  
  type AccountValues {
    id: AccountID!
    sequenceNumber: String!
    numSubentries: Int!
    inflationDestination: Account
    homeDomain: String
    thresholds: AccountThresholds!
    signers: [Signer]
  }
  
  type AllowTrustOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    trustor: Account!
    authorize: Boolean!
    asset: Asset!
  }
  
  type Asset {
    id: AssetID!
    issuer: Account
    code: AssetCode!
    totalSupply: String!
    circulatingSupply: String!
    holdersCount: Int!
    unauthorizedHoldersCount: Int!
    lastModifiedIn: Ledger!
    authRequired: Boolean!
    authRevocable: Boolean!
    authImmutable: Boolean!
    balances(
      first: Int
      last: Int
      after: String
      before: String
    ): BalanceConnection
  }
  
  scalar AssetCode
  
  type AssetConnection {
    pageInfo: PageInfo!
    nodes: [Asset]
    edges: [AssetEdge]
  }
  
  type AssetEdge {
    cursor: String!
    node: Asset
  }
  
  scalar AssetID
  
  type Balance {
    account: Account
    asset: Asset!
    limit: String!
    balance: String!
    spendableBalance: String!
    receivableBalance: String!
    authorized: Boolean!
    ledger: Ledger!
  }
  
  type BalanceConnection {
    pageInfo: PageInfo!
    nodes: [Balance]
    edges: [BalanceEdge]
  }
  
  type BalanceEdge {
    cursor: String!
    node: Balance
  }
  
  type BalanceSubscriptionPayload {
    account: Account!
    asset: Asset!
    mutationType: MutationType!
    values: BalanceValues
  }
  
  type BalanceValues {
    account: Account
    asset: Asset!
    limit: String!
    balance: String!
    authorized: Boolean!
  }
  
  type BumpSequenceOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    bumpTo: Int!
  }
  
  type ChangeTrustOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    limit: String!
    asset: Asset!
  }
  
  type CreateAccountOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    startingBalance: String!
    destination: Account!
  }
  
  type CreatePassiveSellOfferOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    priceComponents: OfferPriceComponents!
    price: String!
    amount: String!
    assetSelling: Asset!
    assetBuying: Asset!
  }
  
  type DataEntry {
    name: String!
    value: String!
    ledger: Ledger!
  }
  
  type DataEntrySubscriptionPayload {
    account: Account!
    name: String!
    mutationType: MutationType!
    values: DataEntryValues
  }
  
  type DataEntryValues {
    account: Account!
    name: String!
    value: String!
    ledger: Ledger!
  }
  
  input DataInput {
    name: String
    value: String
  }
  
  scalar DateTime
  
  input EventInput {
    mutationTypeIn: [MutationType!]
    idEq: AccountID
    idIn: [AccountID!]
  }
  
  type InflationOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
  }
  
  type Ledger {
    seq: LedgerSeq!
    header: LedgerHeader
    transactions(
      first: Int
      last: Int
      before: String
      after: String
    ): TransactionConnection
    operations(
      first: Int
      last: Int
      before: String
      after: String
      order: Order
    ): OperationConnection
    payments(
      first: Int
      last: Int
      before: String
      after: String
    ): OperationConnection
  }
  
  type LedgerHeader {
    ledgerVersion: LedgerSeq!
    previousLedgerHash: String!
    txSetResultHash: String!
    baseFee: Int!
    baseReserve: Int!
    maxTxSetSize: Int!
  }
  
  scalar LedgerSeq
  
  type ManageBuyOfferOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    priceComponents: OfferPriceComponents!
    price: String!
    offerId: String!
    amount: String!
    assetSelling: Asset!
    assetBuying: Asset!
  }
  
  type ManageDatumOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    name: String!
    value: String
  }
  
  type ManageSellOfferOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    priceComponents: OfferPriceComponents!
    price: String!
    offerId: String!
    amount: String!
    assetSelling: Asset!
    assetBuying: Asset!
  }
  
  type Memo {
    value: MemoValue
    type: MemoType!
  }
  
  enum MemoType {
    id
    text
    hash
    return
    none
  }
  
  scalar MemoValue
  
  enum MutationType {
    CREATE
    UPDATE
    REMOVE
  }
  
  type Offer {
    id: OfferID!
    seller: Account!
    selling: Asset!
    buying: Asset!
    amount: String!
    price: String!
    passive: Boolean!
    ledger: Ledger!
    trades(first: Int, after: String, last: Int, before: String): TradeConnection!
  }
  
  type OfferConnection {
    pageInfo: PageInfo!
    nodes: [Offer]
    edges: [OfferEdge]
  }
  
  type OfferEdge {
    cursor: String!
    node: Offer
  }
  
  input OfferEventInput {
    mutationTypeIn: [MutationType!]
    idEq: AccountID
    idIn: [AccountID!]
    buyingAssetEq: AssetID
    sellingAssetEq: AssetID
  }
  
  scalar OfferID
  
  type OfferPriceComponents {
    n: Int!
    d: Int!
  }
  
  type OfferSubscriptionPayload {
    accountID: AccountID!
    mutationType: MutationType!
    offerID: OfferID!
    values: OfferValues
  }
  
  type OfferValues {
    id: OfferID!
    seller: Account!
    selling: Asset!
    buying: Asset!
    amount: String!
    price: String!
    passive: Boolean!
  }
  
  interface Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
  }
  
  type OperationConnection {
    pageInfo: PageInfo!
    nodes: [Operation]
    edges: [OperationEdge]
  }
  
  type OperationEdge {
    cursor: String!
    node: Operation
  }
  
  enum OperationType {
    payment
    setOption
    accountMerge
    allowTrust
    bumpSequence
    changeTrust
    createAccount
    manageDatum
    manageSellOffer
    manageBuyOffer
    createPassiveSellOffer
    pathPayment
    inflation
    pathPaymentStrictSend
  }
  
  enum Order {
    desc
    asc
  }
  
  type OrderBook {
    bids: [OrderBookItem!]
    asks: [OrderBookItem!]
  }
  
  type OrderBookItem {
    price: String!
    amount: String!
  }
  
  type PageInfo {
    startCursor: String
    endCursor: String
  }
  
  type PathPaymentOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    sendMax: String!
    amountSent: String
    amountReceived: String
    destinationAsset: Asset!
    sourceAsset: Asset!
    destinationAccount: Account!
  }
  
  type PathPaymentStrictSendOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    destinationMin: String!
    amountSent: String
    amountReceived: String
    destinationAsset: Asset!
    sourceAsset: Asset!
    destinationAccount: Account!
  }
  
  type PaymentOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    destination: Account!
    asset: Asset!
    amount: String!
  }
  
  type PaymentPath {
    sourceAsset: Asset!
    sourceAmount: String!
    destinationAsset: Asset!
    destinationAmount: String!
    path: [Asset!]
  }
  
  type Query {
    asset(id: AssetID): Asset
    assets(
      code: AssetCode
      issuer: AccountID
      first: Int
      after: String
      last: Int
      before: String
    ): AssetConnection
    account(id: AccountID!): Account
    accounts(
      ids: [AccountID!]
      inflationDestination: AccountID
      homeDomain: String
      data: DataInput
      first: Int
      last: Int
      after: String
      before: String
    ): AccountConnection
    ledger(seq: LedgerSeq!): Ledger!
    ledgers(seq: [LedgerSeq!]): [Ledger]!
    offers(
      selling: AssetCode!
      buying: AssetCode!
      first: Int
      after: String
      last: Int
      before: String
    ): OfferConnection
    tick(selling: AssetID!, buying: AssetID!): Tick
    transaction(id: TransactionHash!): Transaction
    transactions(
      first: Int
      after: String
      last: Int
      before: String
    ): TransactionConnection
    operations(
      first: Int
      after: String
      last: Int
      before: String
    ): OperationConnection
    operation(id: String): Operation
    payments(
      first: Int
      after: String
      last: Int
      before: String
    ): OperationConnection
    orderBook(selling: AssetID!, buying: AssetID!, limit: Int): OrderBook
    findPaymentPaths(
      sourceAccountID: AccountID!
      destinationAsset: AssetID!
      destinationAmount: String!
    ): [PaymentPath!]
    tradeAggregations(
      baseAsset: AssetID!
      counterAsset: AssetID!
      startTime: Int
      endTime: Int
      resolution: Int!
      first: Int
      last: Int
    ): [TradeAggregation!]
    trades(
      baseAsset: AssetID
      counterAsset: AssetID
      offerID: Int
      first: Int
      after: String
      last: Int
      before: String
    ): TradeConnection!
  }
  
  type SetOptionsOperation implements Operation {
    id: String!
    type: OperationType!
    sourceAccount: Account!
    dateTime: DateTime!
    transaction: Transaction!
    clearFlags: Int
    setFlags: Int
    homeDomain: String
    masterWeight: Int
    thresholds: SetOptionsThresholds
    signer: SetOptionsSigner
    inflationDestination: Account
  }
  
  type SetOptionsSigner {
    account: Account
    weight: Int
  }
  
  type SetOptionsThresholds {
    low: Int
    medium: Int
    high: Int
  }
  
  type Signer {
    signer: AccountID!
    weight: Int!
    type: SignerType
  }
  
  enum SignerType {
    ed25519
    preAuthX
    hashX
  }
  
  type Subscription {
    balance(args: EventInput): BalanceSubscriptionPayload
    dataEntry(args: EventInput): DataEntrySubscriptionPayload
    account(args: EventInput): AccountSubscriptionPayload
    ledgerCreated: Ledger
    offer(args: OfferEventInput): OfferSubscriptionPayload
    tick(selling: AssetID, buying: AssetID): Tick
    operations(
      txSource: [AccountID]
      opSource: [AccountID]
      type: [OperationType]
      destination: [AccountID]
      asset: [AssetID]
    ): Operation!
  }
  
  type Tick {
    selling: AssetID
    buying: AssetID
    bestBid: Float
    bestAsk: Float
  }
  
  type TimeBounds {
    minTime: DateTime!
    maxTime: DateTime
  }
  
  type Trade {
    id: String
    ledgerCloseTime: DateTime!
    offer: OfferID
    baseOffer: OfferID
    baseAccount: Account
    baseAmount: Float!
    baseAsset: Asset!
    counterOffer: OfferID
    counterAccount: Account
    counterAmount: Float!
    counterAsset: Asset!
    baseIsSeller: Boolean
    price: Float
  }
  
  type TradeAggregation {
    timeStamp: Int!
    tradeCount: Int!
    baseVolume: Float!
    counterVolume: Float!
    avg: Float!
    high: Float!
    low: Float!
    open: Float!
    close: Float!
  }
  
  type TradeConnection {
    pageInfo: PageInfo!
    nodes: [Trade]
    edges: [TradeEdge]
  }
  
  type TradeEdge {
    cursor: String!
    node: Trade
  }
  
  type Transaction {
    id: TransactionHash!
    ledger: Ledger!
    index: Int!
    memo: Memo
    feeAmount: Int!
    sourceAccount: Account!
    timeBounds: TimeBounds
    feeCharged: Int!
    success: Boolean!
    resultCode: Int!
    operations(
      first: Int
      after: String
      last: Int
      before: String
      order: Order
    ): OperationConnection
    payments(
      first: Int
      after: String
      last: Int
      before: String
    ): OperationConnection
  }
  
  type TransactionConnection {
    pageInfo: PageInfo!
    nodes: [Transaction]
    edges: [TransactionEdge]
  }
  
  type TransactionEdge {
    cursor: String!
    node: Transaction
  }
  
  scalar TransactionHash
`;

export default typeDefs;