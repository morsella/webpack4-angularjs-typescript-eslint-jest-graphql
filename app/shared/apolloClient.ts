import { createHttpLink } from "apollo-link-http";
import 'cross-fetch/polyfill';
import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";

const client = new ApolloClient({
  link: createHttpLink({ uri: "https://pubnet.astrograph.io/graphql", credentials: 'same-origin'}),
  cache: new InMemoryCache()
});

export default client;