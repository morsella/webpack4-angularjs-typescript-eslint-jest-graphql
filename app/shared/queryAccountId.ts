const query = `{
        account(id: "GB6NVEN5HSUBKMYCE5ZOWSK5K23TBWRUQLZY3KNMXUZ3AQ2ESC4MY4AQ") {
          id
          sequenceNumber
          balances {
            asset {
              issuer {
                id
              }
              code
            }
            balance
            limit
            authorized
          }
        }
      }
`;

export default query;