import client from '../shared/apolloClient';
import accountsQuery from '../shared/accounts-query.graphql';
import { AccountsType } from '../interfaces/Data.type';
import { AccountType } from '../interfaces/Account.type';
import accountId from '../shared/accountId.graphql';
import theresholdAccount from '../shared/thresholdsAccountId.graphql';
import noTransactions from '../shared/accounts-query-no-transactions.graphql';
import noTransaction from '../shared/accountId-no-transactions.graphql';

class AccountService {
    account = {} as AccountType;
    constructor() {}
    async getAccountData(id: string): Promise<void | AccountType> {
        const result = await client.query({
            query: noTransaction,
            variables:{
                id: id
            }
        });
        if(result) return result.data.account;
    }
   async getData(select: number): Promise<void | AccountsType> {
       const result = await client.query({
            query: noTransactions,
            variables: {
                first: select
            }
       });
       if(result) return result.data;
    }
    async getAccountThresholds(id: string): Promise<void | AccountType>{
        const result  = await client.query({
            query: theresholdAccount,
            variables: {
                id: id
            }
        });
        if(result) return result.data.account;
    }
    selectedAccount(acc: AccountType, $rootScope: ng.IRootScopeService): void {
        this.account = acc;
        $rootScope.$broadcast('account:updated', this.account);
    }
}
export default AccountService;
