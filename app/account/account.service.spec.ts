import { makeExecutableSchema, addMockFunctionsToSchema, mockServer, MockList } from 'graphql-tools';
import casual from 'casual-browserify';
import 'jest';
import 'jest-fetch-mock';
import 'ts-jest/utils';
import typeDefs from '../shared/typeDefs';
import queryAccountId from '../shared/queryAccountId';

// const resolvers = {
//     Operation: {
//       __resolveType(data) {
//         return data.typename // typename property must be set by your mock functions
//       }
//     }
//   }

// const executeSchema = makeExecutableSchema({typeDefs: typeDefs, resolvers});

// Same mocks object that `addMockFunctionsToSchema` takes above
const preserveResolvers = true

const variables = { id: "GB6NVEN5HSUBKMYCE5ZOWSK5K23TBWRUQLZY3KNMXUZ3AQ2ESC4MY4AQ"}

const mocks = {
  AccountID: () => {
    return variables.id
  },
  AssetCode: () => {
    return "XML"
  },
  account: () => ({    
    sequenceNumber:() => { 
      return casual.integer(0, 20) 
    } 
  }),
  balance: () => {
    return casual.double(0, 1000)
  },
  balances: () => {
    return new MockList([0,1]) 
  }
};
const server = mockServer(typeDefs, mocks, preserveResolvers);

const queryQl = `account(id: "GB6NVEN5HSUBKMYCE5ZOWSK5K23TBWRUQLZY3KNMXUZ3AQ2ESC4MY4AQ") {
  id
  sequenceNumber
  balances {
    asset {
      issuer {
        id
      }
      code
    }
    balance
    limit
    authorized
  }
}
}`;

describe('Check console.warn() output', () => {
    it('calls and returns data to me',  async () => {
       await  server.query(queryAccountId, variables)
        .then(result => {
          console.warn(result);
          if(result && result.data){
            // `expected ${result.data.account.sequenceNumber} to be equal Hello World`
            expect(result.data.account.sequenceNumber).toEqual('Hello World')
          } 
          else {
            console.warn(result);
            expect(result).toEqual({ "data": {"account": null}})
          }
        });
    });
});