import angular, { IRootScopeService } from 'angular';
import AccountService from "./account.service";
import { AccountType } from "../interfaces/Account.type";
import { testData } from '../test/test.dataAccounts';
import { testDataAccount } from '../test/test.dataAccount';

class AccountController {
    private parent: ng.IController;
    private $scope: any;
    private $location: ng.ILocationService;
    private _accountService: AccountService;
    initAccount: AccountType;
    loading: boolean = true;
    initAccountId: string;
    header: string = 'random account:';
    error: string = '';
    selectedAccount = {} as AccountType;
    constructor($scope: any, $location: ng.ILocationService, $rootScope: IRootScopeService){
        this.$scope = $scope
        this.$location = $location
        this.parent = this.$scope.$parent.$ctrl;
        this.initAccount = this.parent.initAccount;
        this._accountService = new AccountService();
        this.initAccountId = this.parent.initAccountId; //some account to start with
        this.$scope.$on('account:updated', (event: any, data: any) => {
            this.initAccount = data;
            this.$scope.$apply();
        });
    }
    
    onInit(): void {
        console.log('Account init');
        this.getAccount(this.initAccountId);
    }
    
    updateAcc(acc: AccountType): void {
        this.initAccount = acc;
    }

    private getAccount(id: string): void {
        this._accountService.getAccountData(id).then(res => {
            if(res && res.sequenceNumber) {
                this.initAccount = res;
                this.reset();
                this.$scope.$apply();
            }
        }).catch(err => {
            console.log('ERROR: ', err);
            this.getMockData();
        });
    }
    private getMockData(): void {
        console.log('server is down fetching mock data');
        this.initAccount = testDataAccount.account;
        this.reset();
        this.error = 'This is mock data - Astrograph demo server is down';
    }
    private reset(): void {
        this.error = "";
        this.loading = false;
    }
}
export default AccountController;