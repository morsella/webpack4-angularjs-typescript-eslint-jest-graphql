const template = `
                <div ng-controller="AccountController as ctrl" ng-init="ctrl.onInit()">              
                    <div ng-if="!ctrl.loading" class="account_component">
                        <div id="selectedAccount"  class="account_box col-md-12">
                            <h4 class="col-md-12"> {{ctrl.header}} </h4>
                            <div class="account col-md-12"> account number: {{ctrl.initAccount.sequenceNumber}} </div>
                            <div ng-repeat="initBalance in ctrl.initAccount.balances" class="flex">
                                <table class="flex-item">
                                    <tbody>
                                        <tr>
                                            <th>asset: </th>
                                            <th>balance: </th>
                                            <th>limit: </th>
                                        </tr>
                                        <tr>
                                            <td> {{initBalance.asset.code}} </td>
                                            <td> {{initBalance.balance}} </td>
                                            <td> {{initBalance.limit}}  </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"> authorized: {{initBalance.authorized}} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div ng-if="ctrl.initAccount.numSubentries" class="flex-item"> subentries: {{ctrl.initAccount.numSubentries}} </div>
                            <div ng-if="ctrl.initAccount.thresholds.high" class="flex-item"> thresholds hight: {{ctrl.initAccount.thresholds.high}} </div>
                            <div ng-if="ctrl.initAccount.thresholds.low" class="flex-item"> thresholds low: {{ctrl.initAccount.thresholds.low}} </div>
                        </div>
                    </div>
                </div>{{ctrl.loading}}{{ctrl.error}}
                `;
export default template;