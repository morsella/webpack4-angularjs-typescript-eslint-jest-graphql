import angular from 'angular';
import AccountController from './account.controller';
import AccountService from './account.service';
import template from './account.template';

export default angular.module('accountModule').factory('accountService', AccountService)
                            .controller('AccountController',($scope, $location, $rootScope) => new AccountController($scope, $location, $rootScope))
                            .component('accountComponent', {
                                bindings:{
                                    initAccount: '=',
                                    initAccountId: '@'
                                },
                                template: template,
                                controller: AccountController
                            });