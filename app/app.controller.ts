import { IScope } from 'angular';

class AppController {
  private scope: IScope;
  loading: boolean = true;
  paragraph: string = 'Webpack 4 boilerplate setup with AngularJS, TypeScript, ESlint, GraphQL, ApolloClient, Jest and MockServer using Astrograph Public API...';
  constructor($scope: ng.IScope) {
    this.scope = $scope;
  }
  onInit(): any {
    console.log('App Init');
  }
  loaded(): void {
    console.log('called loaded');
    this.loading = false;
    this.scope.$apply();
  }
}
export default AppController;