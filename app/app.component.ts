import angular from 'angular';
import AppController from './app.controller';
export default angular.module('appModule')
            .controller('AppController', ($scope) => new AppController ($scope))
            .component('appComponent', {
                template: `
                <div ng-controller="AppController as ctrl" ng-init="ctrl.onInit()" class="container">
                  <div class="flex">
                    <img class="logo" src="./images/logo.png" alt="logo">
                    <h1> Astrograph Accounts GraphQL Demo </h1>
                  </div> 
                  <p>{{ctrl.paragraph}}</p>
                  <div class="flex row">
                  <div ng-if="ctrl.loading" class="loader col-md-12">
                    <img src="./images/831.svg" alt="loader" />
                    <p>Fetching accounts data from astrograph - Relax and enjoy your coffee.</p>
                  </div>
                  <div> <accounts-component loaded="ctrl.loaded()" class="accounts_container flex-item" /> </div>
                  <div ng-if="!ctrl.loading" class="flex-item documentation">
                    <div class="astrograph_link"><a href="https://pubnet.astrograph.io/graphql" title="astrograph API Playground" target="blank">Astrograph API Playground - https://pubnet.astrograph.io/graphql</a></div>

                    <p>Webpack 4 boilerplate setup with AngularJS, TypeScript, ESlint, GraphQL, ApolloClient, Jest and MockServer using Astrograph Public API...
                    Astrograph provides very good documented API. Unfortunately the data that is received is not always consistent, but it covers most of the data types and helps to provision the data if you need to build an interface and your backend is not yet ready. 
                    For example when I could fetch transactions one day, the next day the API would return an error saying that some data can't be fetched. 
                    To overcome this issue while builing an interface, I've created a mock data out of the API and when the connection was Error, the data would load the mock data.
                    I tried to make the boiler plate with all the updated and recent dependencies. The only issue that became more problematic is that sometimes Jest fails to read .graphql files. 
                    I really hope that soon there will be updates from Jest, GraphQL or Webpack to solve this. If not I do believe Jasmine is the better option. 
                    TypeScript is set to strict mode and all the Angular-js 1.7.. files are written in TypeScript. 
                    TypeScript is powerful and makes it more secure and efficient in OOP. 
                    With the new updates that Angular community releases, I like the adaptivity and simplicity and it reminds me a bit of React. 
                    I didn't include RxJs in this setup - but it is something that definitely can be added, depends on project's growth. 
                    
                    I keep this little project public for anyone to use it. 
                    
                    There are 3 simple scripts in package.json
                    - npm run build: build the package and also include TypesScript checks.
                    - npm run lint: eslint tests. 
                    - npm run test: jest test.
                    
                    Below you will find a link to the repository.
                    You are welcome to clone the repository and use it.
                    Keep it simple and keep coding :) 
                    
                    M. Sella</p>
                    <h4>Diagram: </h4>
                    <img src="./images/astrograph_demo-2.svg" alt="diagram" class="diargam" />
                  </div>
                  </div>
                  <div ng-if="!ctrl.loading" class="col-md-12">
                    <a  href="https://gitlab.com/morsella/webpack4-angularjs-typescript-eslint-jest-graphql" title="link to repository" target="blank">Gitlab Link to repository</a>
                  </div>
                </div>`,
                controller: AppController
            });
