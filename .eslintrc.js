module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "amd": true,
        "node": true
    },
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "plugins": [
        "angular",
        "jest",
        "@typescript-eslint"
    ],
    "rules": {
        '@typescript-eslint/explicit-member-accessibility': 'off',
        'brace-style': [
            'error',
            'stroustrup'
        ],
        'comma-dangle': [
            'error',
            'never'
        ],
        'no-unused-vars': 'off',
        'no-var': [
            'off'
        ],
        'one-var': [
            'off'
        ]
    },
    overrides: [
        {
          // enable the rule specifically for TypeScript files
          'files': ["*.ts", "*.tsx"],
          'rules': {
            '@typescript-eslint/explicit-member-accessibility': [
                'warn',
                {
                  accessibility: 'no-public',
                }
            ]
          }
        }
      ]
};