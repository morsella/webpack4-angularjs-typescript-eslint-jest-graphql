const webpack = require('webpack');
const path = require('path');

const plugins = [

  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery'
  }),

  new webpack.LoaderOptionsPlugin({
    options: {
      context: __dirname
    }
  })
];

const rules = [
  { 
    test: /\.html$/,
    exclude: /node_modules/,
    use: { loader: "raw-loader" }
  },
  { 
    test: /\.css$/i,
    use: ['style-loader', 'css-loader']
  },
  {
  test: /\.(png|jpg|svg|gif)$/,
  use: ['file-loader']
  },
  { 
    test: /\.(graphql|gql)$/,
    loader: 'graphql-tag/loader'
  },
  {
    test: /.jsx?$/,
    loader: 'babel-loader',
    include: path.join(__dirname, 'app')
  },
  { test: /\.ts(x?)$/, loader: 'ts-loader' },
  {
    test: /\.(jpe?g|png|gif|woff|woff2|svg|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
    loader: 'url-loader?limit=100000',
    include: path.resolve(__dirname, 'dist/assets')
  },
  {
    test: /\.svg$/,
    use: [
      'file-loader',
      'svg-transform-loader'
    ]
  }
];

const config = {
  mode: 'development',
  context: path.join(__dirname, 'app'),
  entry: {
    app: './index.ts'
  },
  output: {
    path: `${__dirname}/app`,
    filename: './bundle.js'
  },
  plugins,
  resolve: {
    extensions: ['.ts', '.js']
  },
  node: {
    fs: 'empty',
    tls: 'empty'
  },
  module: {
    rules
  },
  devServer: {
    proxy: {
      '/api/*': {
        target: 'http://localhost:3000'
      }
    },
    host: 'localhost'
  },
  optimization: {
    minimize: false
  }
};

if (process.env.NODE_ENV === 'production') {
  config.output.path = `${__dirname}/dist`;
}
else {
  config.devtool = 'eval';
}

module.exports = config;